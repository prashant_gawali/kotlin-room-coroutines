package com.example.mynotes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.mynotes.db.Note
import com.example.mynotes.db.NoteDatabase
import com.example.mynotes.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_add_note.*
import kotlinx.coroutines.launch

class AddNoteFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_note, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_save.setOnClickListener {
            val noteTitle = notetitle.text.toString().trim()
            val noteDesc = notedesc.text.toString().trim()

            if (noteTitle.isEmpty()) {
                notetitle.error = "Title Reequired"
                notetitle.requestFocus()
                return@setOnClickListener
            }
            if (noteDesc.isEmpty()) {

                notedesc.error = "Description Reequired"
                notedesc.requestFocus()
                return@setOnClickListener

            }
            launch {
                val note = Note(noteTitle, noteDesc)
                context?.let {
                    NoteDatabase(it).getNoteDao().addNote(note)
                    Toast.makeText(context, "Note Saved", Toast.LENGTH_SHORT).show()

                }
            }

        }
    }


}